/***************************************************************
 * Name:      App.cpp
 * Purpose:   Code for the application class
 * Author:    Artur Gulik (artur.gulik@protonmail.com)
 * Created:   2021-07-19
 * Copyright: Artur Gulik ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#ifdef __BORLANDC__
#pragma hdrstop
#endif //__BORLANDC__

#include "App.h"
#include "Frame.h"

IMPLEMENT_APP(App);

bool App::OnInit()
{

    Frame* frame = new Frame(nullptr, "Odtmaker");
    frame->CenterOnScreen();
    frame->Show();
    return true;
}
