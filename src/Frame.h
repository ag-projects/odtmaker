/***************************************************************
 * Name:      Frame.h
 * Purpose:   Declares the main application frame
 * Author:    Artur Gulik (artur.gulik@protonmail.com)
 * Created:   2021-07-19
 * Copyright: Artur Gulik ()
 * License:
 **************************************************************/

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "App.h"

class Frame: public wxFrame
{
    public:
        Frame(wxFrame *frame, const wxString& title);
        ~Frame();
    private:
        enum
        {
            idMenuQuit = 1000,
            idMenuNew = 1001,
            idMenuAbout = 1002,
            idMenuAdditional = 1003,
	    idMenuSettings = 1004
        };
        std::string language[35];
        std::string dotpath = "";
        std::string default_language = "";
        std::string template_path = "";
        wxStaticText *text0;

        wxStaticText *text1;
        wxTextCtrl *textctrl1 = new wxTextCtrl(this, wxID_ANY, "", wxPoint(240,60), wxSize(120,30));

        wxStaticText *text2 = new wxStaticText(this, wxID_ANY, "Dane hodowcy:", wxPoint(20,120), wxSize(200,30));
        wxTextCtrl *textctrl2 = new wxTextCtrl(this, wxID_ANY, "", wxPoint(240,120), wxSize(120,30));

        wxStaticText *text3 = new wxStaticText(this, wxID_ANY, "Dane kupujacego:", wxPoint(20,180), wxSize(200,30));
        wxTextCtrl *textctrl3 = new wxTextCtrl(this, wxID_ANY, "", wxPoint(240,180), wxSize(120,30));

        wxButton *button = new wxButton(this, 10001, "Generuj dokument", wxPoint(200,250), wxSize(180,40));
	 wxComboBox *mytemplatebox;

        void OnNew(wxCommandEvent& event);
        void OnClose(wxCloseEvent& event);
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        void OnAdditional(wxCommandEvent &event);
        void OnClickButton(wxCommandEvent& event);
        void getLanguagePack(std::string langpath, std::string language_name);
        void OnSettings(wxCommandEvent& event);
        DECLARE_EVENT_TABLE()
};

