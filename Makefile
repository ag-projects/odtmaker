VERSION=0.1.0

CC=g++
WX_CXXFLAGS:=$(shell wx-config --cxxflags)
WX_LIBS:=$(shell wx-config --libs)


#installation directories
prefix=/usr/local
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin

datarootdir=$(prefix)/share
datadir=$(datarootdir)

configdir=${HOME}/.config/odtmaker
languagedir=$(datadir)/odtmaker/languages
exampletemplatedir=$(datadir)/odtmaker/example-templates

#compiler flags
CFLAGS=-Wall -g
OBJ_COMPFLAGS=-c $(CFLAGS)
OBJ_DEFINES=-DLANGUAGE_DIR=\"$(languagedir)\" -DEXAMPLE_TEMPLATE_DIR=\"$(exampletemplatedir)\" -DAPP_VERSION=\"$(VERSION)\" -DICON_PATH=\"\"


odtmaker: App.o Frame.o
	$(CC) ./obj/App.o ./obj/Frame.o $(WX_LIBS) -Wl,--rpath,/usr/local/lib/ $(CFLAGS) -o odtmaker

App.o: src/App.cpp src/App.h src/Frame.h
	$(CC) $(WX_CXXFLAGS) $(OBJ_COMPFLAGS) $(OBJ_DEFINES) ./src/App.cpp -o ./obj/App.o

Frame.o: src/Frame.cpp src/Frame.h
	$(CC) $(WX_CXXFLAGS) $(OBJ_COMPFLAGS) $(OBJ_DEFINES) ./src/Frame.cpp -o ./obj/Frame.o

clean:
	rm -f odtmaker ./obj/*.o

info:
	echo $(exec_prefix)

test: tApp.o tFrame.o
	$(CC) ./obj/tApp.o ./obj/tFrame.o $(WX_LIBS) -Wl,--rpath,/usr/local/lib/ $(CFLAGS) -o odtmaker

tApp.o: src/App.cpp src/App.h src/Frame.h
	$(CC) $(WX_CXXFLAGS) $(OBJ_COMPFLAGS) -DLANGUAGE_DIR=\"languages\" -DAPP_VERSION=\"$(VERSION)\" -DICON_PATH=\"icon\" ./src/App.cpp -o ./obj/tApp.o

tFrame.o: src/Frame.cpp src/Frame.h
	$(CC) $(WX_CXXFLAGS) $(OBJ_COMPFLAGS) -DLANGUAGE_DIR=\"languages\" -DAPP_VERSION=\"$(VERSION)\" -DICON_PATH=\"icon\" ./src/Frame.cpp -o ./obj/tFrame.o

install: odtmaker
	install -d $(DESTDIR)$(bindir)/
	install -m 755 ./odtmaker $(DESTDIR)$(bindir)/
	install -d $(configdir)
	install -m 644 unixlib.h $(DESTDIR)$(PREFIX)/include/
