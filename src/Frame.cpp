/***************************************************************
 * Name:      Frame.cpp
 * Purpose:   Code for the main application frame
 * Author:    Artur Gulik (artur.gulik@protonmail.com)
 * Created:   2021-07-19
 * Copyright: Artur Gulik ()
 * License:
 **************************************************************/

#ifdef WX_PRECOMP
#include "wx_pch.h"
#endif

#include "Frame.h"

#include <libintl.h>
#include <fstream>

#include <wx/popupwin.h>
#include <wx/aboutdlg.h>

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__WXMAC__)
        wxbuild << _T("-Mac");
#elif defined(__UNIX__)
        wxbuild << _T("-Unix-Like");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

std::string getDotpath() {
        char path[PATH_MAX];
        ssize_t length = ::readlink("/proc/self/exe", path, sizeof(path)-1);
        if (length != -1) {
          path[length] = '\0';
          std::string final_path = std::string(path);
          if (final_path == "/" || final_path == "") return "";
          final_path = final_path.substr(0, final_path.rfind("/"));
          return final_path;
        }
        //on error
        wxString msg = "The executable's path couldn't be found. Assuming it is in `pwd`";
        wxMessageBox(msg, _("Error"));
        return std::string(".");
}

std::string getDefaultLanguage(std::string dotpath){
    std::string result = "english";
    std::ifstream default_language_file;
    default_language_file.open(dotpath + "/languages/default");

    if (default_language_file.good()){
        getline(default_language_file, result);
    }
    return result;
}


BEGIN_EVENT_TABLE(Frame, wxFrame)
    EVT_CLOSE(Frame::OnClose)
    EVT_MENU(idMenuNew, Frame::OnNew)
    EVT_MENU(idMenuQuit, Frame::OnQuit)
    EVT_MENU(idMenuAbout, Frame::OnAbout)
    EVT_MENU(idMenuAdditional, Frame::OnAdditional)
    EVT_MENU(idMenuSettings, Frame::OnSettings)
    EVT_BUTTON(10001, Frame::OnClickButton)
END_EVENT_TABLE()

Frame::Frame(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title, wxDefaultPosition, wxSize(1200,700))
{
  //std::cout<<LANGUAGE_DIR<<std::endl;
  getLanguagePack(LANGUAGE_DIR, "english");
#if wxUSE_MENUS
    // create a menu bar
    std::string menu[10][10];
    std::string description[10][10];
    //File
    menu[0][0] = "&";
    menu[0][0].append(language[0]);
    	//New
    	menu[0][1] = "&";
	menu[0][1].append(language[1]);
	menu[0][1].append("\tCtrl-n");
	description[0][1] = language[2];
	//Settings
	menu[0][2] = "&";
	menu[0][2].append(language[11]);
	menu[0][2].append("\tCtrl-e");
	description[0][2] = language[12];
	//Exit
	menu[0][3] = "&";
	menu[0][3].append(language[3]);
	menu[0][3].append("\tAlt-F4");
	description[0][3] = language[4];
    //Info
    menu[1][0] = "&";
    menu[1][0].append(language[5]);
    	//About this app
        menu[1][1] = "&";
	menu[1][1].append(language[6]);
	menu[1][1].append("\tF1");
	description[1][1] = language[7];
	//About author
	menu[1][2] = "&";
	menu[1][2].append(language[8]);
	menu[1][2].append("\tF2");
	description[1][2] = language[9];
	
    wxMenuBar* mbar = new wxMenuBar();

    wxMenu* fileMenu = new wxMenu("");
        fileMenu->Append(idMenuNew, menu[0][1], description[0][1]);
	fileMenu->Append(idMenuSettings, menu[0][2], description[0][2]);
    	fileMenu->Append(idMenuQuit, menu[0][3], description[0][3]);

    wxMenu* helpMenu = new wxMenu("");
    	helpMenu->Append(idMenuAbout, menu[1][1], description[1][1]);
    	helpMenu->Append(idMenuAdditional, menu[1][2], description[1][2]);

    mbar->Append(fileMenu, menu[0][0]);
    mbar->Append(helpMenu, menu[1][0]);


    SetMenuBar(mbar);
#endif // wxUSE_MENUS

#if wxUSE_STATUSBAR
    CreateStatusBar(1);
    SetStatusText(language[10],0);
    //SetStatusText(wxbuildinfo(short_f), 1);
#endif // wxUSE_STATUSBAR

    //still the Frame constructor:

    //this->SetLabel("gulas");

    dotpath = getDotpath();
    std::cout<<dotpath<<std::endl;
    default_language = getDefaultLanguage(dotpath);

    //getLanguagePack(dotpath+default_language, default_language);




    //"Wypelnij ponizszy formularz i kliknij przycisk 'Generuj dokument'"
    //"Nazwa dokumentu\n(bez rozszerzenia):"
    text0 = new wxStaticText(this, wxID_ANY, language[0], wxPoint(20,20), wxSize(760,30),wxALIGN_CENTRE_HORIZONTAL);
    text1 = new wxStaticText(this, wxID_ANY, language[1], wxPoint(20,60), wxSize(200,50));

    std::string default_template = "";
    std::ifstream default_template_file;

    default_template_file.open(dotpath + "/templates/default");
    if (default_template_file.good()){
        getline(default_template_file, default_template);
    }
    default_template_file.close();

    wxArrayString choices;

    long result = wxExecute("ls " + dotpath + "/templates/", choices, wxEXEC_SYNC);
    if (result){ //errors occured during the command execution
        //err
    }
    if (choices.Index("default") != wxNOT_FOUND) choices.Remove("default"); //if there's a 'default' file we don't want to display it as a template option

    mytemplatebox = new wxComboBox(this, wxID_ANY, default_template, wxPoint(10,100), wxSize(100,80), choices, wxCB_READONLY);
}

Frame::~Frame()
{
}

void Frame::OnClose(wxCloseEvent &event)
{
    Destroy();
}

void Frame::OnQuit(wxCommandEvent &event)
{
    Destroy();
}

void Frame::OnAbout(wxCommandEvent &event)
{
   wxString msg = language[13] +
     "\nBuild info: " + wxbuildinfo(long_f);

    wxAboutDialogInfo aboutInfo;
    aboutInfo.SetName("Odtmaker");
    aboutInfo.SetVersion(APP_VERSION);
    aboutInfo.SetDescription(msg);
    aboutInfo.SetCopyright("(C) 2021 Artur Gulik\nartur.gulik@protonmail.com");
    aboutInfo.SetWebSite("https://gitlab.com/ag-projects/odtmaker");
    aboutInfo.AddDeveloper("Artur Gulik");
    aboutInfo.SetLicense("GPL3");
    
    wxString iconpath = ICON_PATH;
    iconpath += "/odtmaker_64x64.png";
    wxIcon myIcon(iconpath);
    aboutInfo.SetIcon(myIcon);
    wxAboutBox(aboutInfo, this);
    
}

void Frame::OnAdditional(wxCommandEvent &event)
{
    wxString msg = language[15] + "\n" + language [16];
    wxMessageBox(msg, language[14], wxOK);
}

void Frame::OnSettings(wxCommandEvent &event)
{
  wxArrayString choices(10);
  wxString search_pattern = LANGUAGE_DIR;
  search_pattern.append("/*.language");
  wxString file = wxFindFirstFile(search_pattern);
  while (!file.empty()){
    choices.Add(file);
    file = wxFindNextFile();
  }
  if (choices.GetCount() == 0){
	wxString msg = "No language packs were found. Make sure that the following directory:\n";
	msg.append(LANGUAGE_DIR);
	msg.append("\ncontains files with .language extension");
	wxMessageBox(msg, "Error - no language packs", wxICON_ERROR);
  }
  else{
    int a = wxGetSingleChoiceIndex("Set the application language", "caption", choices, this);
  }

}


void Frame::OnNew(wxCommandEvent &event)
{
    wxString msg = "Aby utworzyc nowy dokument nalezy wybrac szablon";
    wxMessageBox(msg, _("Nowy dokument"), wxOK);
    template_path = wxDirSelector("Choose a template directory", dotpath + "/templates");
    std::cout<<"A template has been chosen: " + template_path<<std::endl;
}


void Frame::OnClickButton(wxCommandEvent &event)
{
    //std::cout<<dotpath<<std::endl;


    wxString chosen_filename = textctrl1->GetValue();
    wxString output_path = dotpath + "/" + chosen_filename; //path to the output dir before zipping
    if (template_path == ""){
        wxString msg = "Aby utworzyc nowy dokument nalezy wybrac szablon";
        wxMessageBox(msg, _("Nie wybrano szablonu"), wxOK);
        return;
    }

    //std::system("mkdir " + output_path);
    //std::system("cp -r " + template_path + "/* " + output_path); //copy template files to the output path

    std::ifstream in;
    in.open(template_path + "/content.xml");

    std::ofstream out;
    out.open(output_path + "/content.xml", std::ofstream::trunc);

    std::string line;
    while (getline(in,line)){
        for (unsigned int i=2;i<line.length();++i){
            if ((line[i] == ']') && (line[i-2] == '[')){
                line = line.substr(0,i-2) + "This was found as field no. " + line[i-1] + line.substr(i+1,line.length()-1);
            }
        }
        out<<line<<std::endl;
    }
}

void Frame::getLanguagePack(std::string langpath, std::string language_name){
    //this function sets the Frame::language array
    std::ifstream language_file;
    language_file.open(langpath + "/" + language_name);
    //    std::cout<<langpath + "/" + language_name<<std::endl;
    if (!language_file.good()) {
      std::cout<<"odtmaker: failed to load the language pack\n";
      language_file.close();
      language[0] = "0File";
      language[1] = "1New";
      language[2] = "2Desc";

      return;
    }
    for (int i=0;i<25;++i){
        getline(language_file, language[i]);
    }
    language_file.close();
    return;
    
}


