/***************************************************************
 * Name:      App.h
 * Purpose:   Declares the application class
 * Author:    Artur Gulik (artur.gulik@protonmail.com)
 * Created:   2021-07-19
 * Copyright: Artur Gulik ()
 * License:
 **************************************************************/

#ifndef App_H
#define App_H

#include <wx/app.h>

class App : public wxApp
{
    public:
        virtual bool OnInit();
};
#endif // App_H
